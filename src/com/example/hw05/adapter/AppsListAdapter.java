package com.example.hw05.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hw05.R;
import com.example.hw05.model.ItunesApp;
import com.squareup.picasso.Picasso;

/*
 * HW05
 * AppsListAdapter.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class AppsListAdapter extends ArrayAdapter<ItunesApp> {

	Context context;
	int resource;
	List<ItunesApp> appsList;

	public AppsListAdapter(Context context, int resource, List<ItunesApp> objects) {
		super(context, resource, objects);

		this.context = context;
		this.resource = resource;
		this.appsList = objects;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(resource, parent, false);
			Holder holder = new Holder();
			holder.imageView = (ImageView) convertView.findViewById(R.id.imageView);
			holder.title = (TextView) convertView.findViewById(R.id.appTitleTextView);
			holder.devName = (TextView) convertView.findViewById(R.id.devNameTextView);
			holder.releaseDate = (TextView) convertView.findViewById(R.id.releaseDateTextView);
			holder.price = (TextView) convertView.findViewById(R.id.priceTextView);
			convertView.setTag(holder);
		}

		Holder holder = (Holder) convertView.getTag();
		ItunesApp app = appsList.get(position);
		Picasso.with(context).load(app.getSmallPhoto()).into(holder.imageView);
		holder.title.setText(app.getTitle());
		holder.devName.setText(app.getDevName());
		holder.releaseDate.setText(app.getReleaseDate());
		holder.price.setText(app.getPrice());

		return convertView;
	}

	class Holder {
		public ImageView imageView;
		public TextView title;
		public TextView devName;
		public TextView releaseDate;
		public TextView price;
	}

}
