package com.example.hw05.adapter;

import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.hw05.model.User;
import com.example.hw05.util.UserComparator;

/*
 * HW05
 * UserNameAdapter.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class UserNameAdapter extends ArrayAdapter<User> {

	Context context;
	int resource;
	List<User> users;
	public UserNameAdapter(Context context, int resource, List<User> objects) {
		super(context, resource, objects);
		this.context = context;
		this.resource = resource;
		this.users = objects;
		Collections.sort(this.users, new UserComparator());
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(resource, parent, false);
		}
		
		TextView text1 = (TextView) convertView.findViewById(android.R.id.text1);
		text1.setText(users.get(position).getFirstName() + " " + users.get(position).getLastName());
		return convertView;
	}

}
