package com.example.hw05;

import java.util.List;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

/*
 * HW05
 * BaseAppsActivity.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class BaseAppsActivity extends BaseActivity {

	public static final String TB_ITUNESAPP = "ItunesApp";
	public static final String TB_ITUNESAPP_RELEASE_DATE = "Release_Date";
	public static final String TB_ITUNESAPP_PRICE = "Price";
	public static final String TB_ITUNESAPP_LARGE_PHOTO = "Large_Photo";
	public static final String TB_ITUNESAPP_SMALL_PHOTO = "Small_Photo";
	public static final String TB_ITUNESAPP_DEVELOPER_NAME = "Developer_Name";
	public static final String TB_ITUNESAPP_URL = "Url";
	public static final String TB_ITUNESAPP_TITLE = "Title";
	public static final String TB_ITUNESAPP_ID = "Id";

	public static final String TB_USER = "_User";
	public static final String TB_USER_LAST_NAME = "Last_Name";
	public static final String TB_USER_FIRST_NAME = "First_Name";
	public static final String TB_USER_USERNAME = "username";
	public static final String TB_USER_EMAIL = "email";

	public static final String TB_FAVORITES = "Favorites";
	public static final String TB_FAVORITES_USER = "User";
	public static final String TB_FAVORITES_ITUNES_APP = "ItunesApp";

	public static final String TB_SHARED = "SharedWith";
	public static final String TB_SHARED_USER = "User";
	public static final String TB_SHARED_ITUNES_APP = "ItunesApp";
	
	protected static final int FAV_APP_ACTIVITY = 11;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_base_apps);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.preview_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();

		if (id == R.id.viewAll) {
			Intent intent = new Intent(this, AppsActivity.class);
			startActivity(intent);
			finish();
		} else if (id == R.id.viewFav) {
			Intent intent = new Intent(this, FavoriteAppsActivity.class);
			startActivity(intent);
			finish();
		} else if (id == R.id.clearFav) {
			ParseQuery<ParseObject> favQuery = new ParseQuery<ParseObject>(TB_FAVORITES);
			favQuery.whereEqualTo(TB_FAVORITES_USER, ParseUser.getCurrentUser());
			favQuery.findInBackground(new FindCallback<ParseObject>() {

				@Override
				public void done(List<ParseObject> objects, ParseException e) {
					if (e == null) {
						ParseObject.deleteAllInBackground(objects);
						favoritesCleared();
					}
				}
			});
		} else if (id == R.id.viewShared) {
			Intent intent = new Intent(this, SharedAppsActivity.class);
			startActivity(intent);
			finish();
		} else if (id == R.id.clearShared) {
			ParseQuery<ParseObject> sharedQuery = new ParseQuery<ParseObject>(TB_SHARED);
			sharedQuery.whereEqualTo(TB_SHARED_USER, ParseUser.getCurrentUser());
			sharedQuery.findInBackground(new FindCallback<ParseObject>() {

				@Override
				public void done(List<ParseObject> objects, ParseException e) {
					if(e == null) {
						ParseObject.deleteAllInBackground(objects);
						sharedCleared();
					}
				}
			});
		} else if (id == R.id.logout) {
			ParseUser.logOut();
			Intent intent = new Intent(this, LoginActivity.class);
			startActivity(intent);
			finish();
		}

		return super.onOptionsItemSelected(item);
	}

	protected void favoritesCleared() {
		
	}
	
	protected void sharedCleared() {
		
	}
}
