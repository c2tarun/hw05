package com.example.hw05;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hw05.util.GeneralUtil;

/*
 * HW05
 * BaseActivity.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class BaseActivity extends Activity {
	
	protected boolean error = false;
	protected static final String ITUNES_API = "https://itunes.apple.com/us/rss/topgrossingapplications/limit=100/xml";
	protected static final String PREVIEW_APP = "Itunes app to preview";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_base);
	}
	
	public void validateFields(EditText... editTexts) {
		error = false;
		for(EditText editText : editTexts) {
			if(isValid(editText)) {
				continue;
			} else {
				error = true;
				return;
			}
		}
	}

	/**
	 * return false if empty
	 * 
	 * @param editText
	 * @return
	 */
	public boolean isValid(EditText editText) {
		String str = editText.getText().toString();

		if (GeneralUtil.isEmpty(str)) {
			Toast.makeText(getApplicationContext(), editText.getHint() + " Cannot be empty!", Toast.LENGTH_SHORT).show();
			return false;
		}

		return true;
	}
}
