package com.example.hw05;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.xml.sax.SAXException;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.hw05.adapter.AppsListAdapter;
import com.example.hw05.model.ItunesApp;
import com.example.hw05.util.HttpUtil;
import com.example.hw05.util.ItunesAppUtil;

/*
 * HW05
 * AppsActivity.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class AppsActivity extends BaseAppsActivity {

	ProgressDialog pd;
	ListView appsListView;
	List<ItunesApp> appsList;
	private static final String TAG = AppsActivity.class.getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_apps);
		appsListView = (ListView) findViewById(R.id.listView1);
		
		appsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				ItunesApp app = appsList.get(position);
				Intent intent = new Intent(AppsActivity.this, PreviewActivity.class);
				intent.putExtra(PREVIEW_APP, app);
				startActivity(intent);
			}
		});
		
		
		new FetchItunesApp().execute(ITUNES_API);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		if(id == R.id.viewAll) {
			
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	protected void populateListView() {
		AppsListAdapter adapter = new AppsListAdapter(this, R.layout.app_list_layout, appsList);
		appsListView.setAdapter(adapter);
	}

	class FetchItunesApp extends AsyncTask<String, Void, List<ItunesApp>> {

		@Override
		protected void onPreExecute() {
			pd = new ProgressDialog(AppsActivity.this);
			pd.setTitle("Loading");
			pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			pd.setCancelable(false);
			pd.setMessage("Loading Results...");
			pd.show();
		}

		@Override
		protected List<ItunesApp> doInBackground(String... params) {
			InputStream in = HttpUtil.getInputStream(params);
			try {
				return ItunesAppUtil.ItunesAppSaxParser.parseItunesApp(in);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(List<ItunesApp> result) {
			appsList = result;
			populateListView();
			pd.dismiss();
		}

	}

}
