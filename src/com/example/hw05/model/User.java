package com.example.hw05.model;

import java.io.Serializable;

import com.example.hw05.PreviewActivity;
import com.parse.ParseObject;

/*
 * HW05
 * User.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class User implements Serializable {

	private String username;
	private String firstName;
	private String lastName;
	private String email;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "User [username=" + username + ", firstName=" + firstName + ", lastName=" + lastName + ", email="
				+ email + "]";
	}
	
	public static User getUserFromParseObject(ParseObject parseObject) {
		User user = new User();
		user.setUsername(parseObject.getString(PreviewActivity.TB_USER_USERNAME));
		user.setFirstName(parseObject.getString(PreviewActivity.TB_USER_FIRST_NAME));
		user.setLastName(parseObject.getString(PreviewActivity.TB_USER_LAST_NAME));
		user.setEmail(parseObject.getString(PreviewActivity.TB_USER_EMAIL));
		return user;
	}

}
