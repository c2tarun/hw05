package com.example.hw05.model;

import java.io.Serializable;

import com.example.hw05.BaseAppsActivity;
import com.parse.ParseObject;

/*
 * HW05
 * ItunesApp.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class ItunesApp implements Serializable {

	private String id;
	private String title;
	private String url;
	private String devName;
	private String smallPhoto;
	private String largePhoto;
	private String price;
	private String releaseDate;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDevName() {
		return devName;
	}

	public void setDevName(String devName) {
		this.devName = devName;
	}

	public String getSmallPhoto() {
		return smallPhoto;
	}

	public void setSmallPhoto(String smallPhoto) {
		this.smallPhoto = smallPhoto;
	}

	public String getLargePhoto() {
		return largePhoto;
	}

	public void setLargePhoto(String largePhoto) {
		this.largePhoto = largePhoto;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		
		double priceD = Double.parseDouble(price);
		this.price = priceD + "";
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	@Override
	public String toString() {
		return "ItunesApp [id=" + id + "\n title=" + title + "\n url=" + url + "\n devName=" + devName
				+ "\n smallPhoto=" + smallPhoto + "\n largePhoto=" + largePhoto + "\n price=" + price
				+ "\n releaseDate" + releaseDate + "]";
	}
	
	public static ItunesApp getAppFromParseObject(ParseObject object) {
		ItunesApp app = new ItunesApp();
		app.setId(object.getString(BaseAppsActivity.TB_ITUNESAPP_ID));
		app.setTitle(object.getString(BaseAppsActivity.TB_ITUNESAPP_TITLE));
		app.setUrl(object.getString(BaseAppsActivity.TB_ITUNESAPP_URL));
		app.setDevName(object.getString(BaseAppsActivity.TB_ITUNESAPP_DEVELOPER_NAME));
		app.setSmallPhoto(object.getString(BaseAppsActivity.TB_ITUNESAPP_SMALL_PHOTO));
		app.setLargePhoto(object.getString(BaseAppsActivity.TB_ITUNESAPP_LARGE_PHOTO));
		app.setPrice(object.getString(BaseAppsActivity.TB_ITUNESAPP_PRICE));
		app.setReleaseDate(object.getString(BaseAppsActivity.TB_ITUNESAPP_RELEASE_DATE));
		return app;
	}
	
	public static ItunesApp getAppFromFavoriteParseObject(ParseObject object) {
		ParseObject appParseObject = object.getParseObject(BaseAppsActivity.TB_FAVORITES_ITUNES_APP);
		return getAppFromParseObject(appParseObject);
	}

	public static ItunesApp getAppFromSharedParseObject(ParseObject object) {
		ParseObject appParseObject = object.getParseObject(BaseAppsActivity.TB_SHARED_ITUNES_APP);
		return getAppFromParseObject(appParseObject);
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItunesApp other = (ItunesApp) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
