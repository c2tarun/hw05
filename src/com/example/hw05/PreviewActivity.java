package com.example.hw05;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.hw05.adapter.UserNameAdapter;
import com.example.hw05.model.ItunesApp;
import com.example.hw05.model.User;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.squareup.picasso.Picasso;

/*
 * HW05
 * PreviewActivity.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class PreviewActivity extends BaseAppsActivity {

	TextView appTitleTextView;
	ImageView appImageView;
	ImageView favImageView;
	ImageView shareImageView;
	ItunesApp app;
	ParseObject itunesApp;
	ProgressDialog pd;
	protected static final String USERS_POPUP_MSG = "Users";

	private static final String TAG = PreviewActivity.class.getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_preview);

		appTitleTextView = (TextView) findViewById(R.id.appTitleTextView);
		appImageView = (ImageView) findViewById(R.id.imageView);
		favImageView = (ImageView) findViewById(R.id.favImage);
		shareImageView = (ImageView) findViewById(R.id.shareImage);

		if (getIntent().getExtras() != null) {
			app = (ItunesApp) getIntent().getExtras().get(PREVIEW_APP);
			appTitleTextView.setText(app.getTitle());
			Picasso.with(this).load(app.getLargePhoto()).into(appImageView);
			checkAppInFavorites();
			shareImageView.setImageResource(R.drawable.socialshare);
		}

		appImageView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (app != null) {
					Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(app.getUrl()));
					startActivity(browserIntent);
				}
			}
		});

		shareImageView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				UsersDialogBox usersDialog = new UsersDialogBox();
				usersDialog.show(getFragmentManager(), USERS_POPUP_MSG);
			}
		});

		favImageView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				toggleFavorite();
			}
		});
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		if(id == R.id.viewAll) {
			
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		Intent intent = new Intent();
		intent.putExtra(PREVIEW_APP, app);
		setResult(RESULT_OK, intent);
		super.onBackPressed();
	}

	private void checkAppInFavorites() {

		ParseQuery<ParseObject> itunesAppQuery = new ParseQuery<ParseObject>(TB_ITUNESAPP);
		itunesAppQuery.whereEqualTo(TB_ITUNESAPP_ID, app.getId());

		itunesAppQuery.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				if (e == null) {
					if (objects.size() > 0) {
						ParseQuery<ParseObject> favQuery = new ParseQuery<ParseObject>(TB_FAVORITES);
						favQuery.whereEqualTo(TB_FAVORITES_USER, ParseUser.getCurrentUser());
						favQuery.whereEqualTo(TB_FAVORITES_ITUNES_APP, objects.get(0));

						favQuery.findInBackground(new FindCallback<ParseObject>() {

							@Override
							public void done(List<ParseObject> found, ParseException e) {
								if (e == null) {
									if (found != null && found.size() > 0) {
										changeFavoriteImage(true);
									} else {
										changeFavoriteImage(false);
									}
								} else {
									Log.d(TAG, e.getMessage());
								}
							}
						});
					} else {
						changeFavoriteImage(false);
					}
				} else {
					Log.d(TAG, e.getMessage());
					changeFavoriteImage(false);
				}
			}
		});
	}

	private void toggleFavorite() {
		ParseQuery<ParseObject> itunesAppQuery = new ParseQuery<ParseObject>(TB_ITUNESAPP);
		itunesAppQuery.whereEqualTo(TB_ITUNESAPP_ID, app.getId());
		itunesAppQuery.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(final List<ParseObject> appInItunesTable, ParseException e) {
				if (e == null) {
					if (appInItunesTable.size() > 0) { // App found in ItunesApp
														// table
						ParseQuery<ParseObject> favQuery = new ParseQuery<ParseObject>(TB_FAVORITES);
						favQuery.whereEqualTo(TB_FAVORITES_USER, ParseUser.getCurrentUser());
						favQuery.whereEqualTo(TB_FAVORITES_ITUNES_APP, appInItunesTable.get(0));

						favQuery.findInBackground(new FindCallback<ParseObject>() {

							@Override
							public void done(List<ParseObject> objects, ParseException e) {
								if (e == null) {
									if (objects.size() > 0) { // App found in
																// favorites
																// table
										objects.get(0).deleteInBackground();
										changeFavoriteImage(false);
									} else { // App not found in favourites
												// table
										addAppToFavorite(appInItunesTable.get(0));
										changeFavoriteImage(true);
									}
								}
							}
						});

					} else {
						final ParseObject itunesApp = new ParseObject(TB_ITUNESAPP);
						itunesApp.put(TB_ITUNESAPP_ID, app.getId());
						itunesApp.put(TB_ITUNESAPP_TITLE, app.getTitle());
						itunesApp.put(TB_ITUNESAPP_URL, app.getUrl());
						itunesApp.put(TB_ITUNESAPP_DEVELOPER_NAME, app.getDevName());
						itunesApp.put(TB_ITUNESAPP_SMALL_PHOTO, app.getSmallPhoto());
						itunesApp.put(TB_ITUNESAPP_LARGE_PHOTO, app.getLargePhoto());
						itunesApp.put(TB_ITUNESAPP_PRICE, app.getPrice());
						itunesApp.put(TB_ITUNESAPP_RELEASE_DATE, app.getReleaseDate());

						itunesApp.saveInBackground(new SaveCallback() {

							@Override
							public void done(ParseException e) {
								if (e == null) {
									addItunesAppToFavorite(itunesApp);
									changeFavoriteImage(true);
								}
							}
						});
					}
				}
			}
		});

	}

	private void addAppToFavorite(final ParseObject itunesAppParseObject) {
		ParseQuery<ParseObject> favQuery = new ParseQuery<ParseObject>(TB_FAVORITES);
		favQuery.whereEqualTo(TB_FAVORITES_USER, ParseUser.getCurrentUser());
		favQuery.whereEqualTo(TB_FAVORITES_ITUNES_APP, itunesAppParseObject);

		favQuery.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				if (e == null) {
					if (objects.size() == 0) {
						addItunesAppToFavorite(itunesAppParseObject);
					}
				}
			}
		});
		changeFavoriteImage(true);
	}

	private void addItunesAppToFavorite(ParseObject itunesAppParseObject) {
		ParseObject favoriteTable = new ParseObject(TB_FAVORITES);
		favoriteTable.put(TB_FAVORITES_USER, ParseUser.getCurrentUser());
		favoriteTable.put(TB_FAVORITES_ITUNES_APP, itunesAppParseObject);
		favoriteTable.saveInBackground();
	}

	private void changeFavoriteImage(boolean isFav) {
		if (isFav) {
			favImageView.setImageResource(R.drawable.ratingimportant);
		} else {
			favImageView.setImageResource(R.drawable.ratingnotimportant);
		}
	}

	class UsersDialogBox extends DialogFragment {

		List<ParseObject> usersParseObjects;

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			final ListView usersListView = new ListView(getActivity());
			enableProgressDialog();
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setView(usersListView);
			builder.setMessage(USERS_POPUP_MSG);
			final List<User> users = new ArrayList<User>();
			ParseQuery<ParseObject> usersQuery = new ParseQuery<ParseObject>(TB_USER);
			usersQuery.findInBackground(new FindCallback<ParseObject>() {
				@Override
				public void done(List<ParseObject> queriedUsers, ParseException e) {
					Log.d(TAG, queriedUsers.size() + "");
					if (e == null) {
						usersParseObjects = queriedUsers;
						for (ParseObject user : queriedUsers) {
							User newUser = User.getUserFromParseObject(user);
							users.add(newUser);
						}

						UserNameAdapter adapter = new UserNameAdapter(PreviewActivity.this,
								android.R.layout.simple_list_item_1, users);
						usersListView.setAdapter(adapter);
					} else {
						Log.e(TAG, e.getMessage());
					}
					disableProgressDialog();
				}
			});

			usersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
					Log.d(TAG, users.get(position).getLastName());
					if (itunesApp == null) {
						itunesApp = new ParseObject(TB_ITUNESAPP);
						itunesApp.put(TB_ITUNESAPP_ID, app.getId());
						itunesApp.put(TB_ITUNESAPP_TITLE, app.getTitle());
						itunesApp.put(TB_ITUNESAPP_URL, app.getUrl());
						itunesApp.put(TB_ITUNESAPP_DEVELOPER_NAME, app.getDevName());
						itunesApp.put(TB_ITUNESAPP_SMALL_PHOTO, app.getSmallPhoto());
						itunesApp.put(TB_ITUNESAPP_LARGE_PHOTO, app.getLargePhoto());
						itunesApp.put(TB_ITUNESAPP_PRICE, app.getPrice());
						itunesApp.put(TB_ITUNESAPP_RELEASE_DATE, app.getReleaseDate());

						itunesApp.saveInBackground(new SaveCallback() {

							@Override
							public void done(ParseException e) {
								saveAppToShared(position);
							}

						});
					} else {
						saveAppToShared(position);
					}
					dismiss();
				}

				private void saveAppToShared(final int position) {

					ParseQuery<ParseObject> sharedQuery = new ParseQuery<ParseObject>(TB_SHARED);
					sharedQuery.whereEqualTo(TB_SHARED_USER, usersParseObjects.get(position));
					sharedQuery.whereEqualTo(TB_SHARED_ITUNES_APP, itunesApp);

					sharedQuery.findInBackground(new FindCallback<ParseObject>() {

						@Override
						public void done(List<ParseObject> objects, ParseException e) {
							if (e == null && objects.isEmpty()) {
								ParseObject sharedTable = ParseObject.create(TB_SHARED);
								sharedTable.put(TB_SHARED_USER, usersParseObjects.get(position));
								sharedTable.put(TB_SHARED_ITUNES_APP, itunesApp);
								sharedTable.saveInBackground();
							}
						}
					});

				}
			});
			return builder.create();
		}
	}

	private void enableProgressDialog() {
		pd = new ProgressDialog(PreviewActivity.this);
		pd.setTitle("Loading");
		pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		pd.setCancelable(false);
		pd.setMessage("Loading Results...");
		pd.show();
	}

	private void disableProgressDialog() {
		pd.dismiss();
	}

}
