package com.example.hw05;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

/*
 * HW05
 * SignUpActivity.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class SignUpActivity extends BaseActivity {

	EditText firstNameEditText;
	EditText lastNameEditText;
	EditText emailEditText;
	EditText passwordEditText;
	EditText confirmPasswordEditText;
	Button signUpButton;
	Button cancelButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_up);

		firstNameEditText = (EditText) findViewById(R.id.firstNameEditText);
		lastNameEditText = (EditText) findViewById(R.id.lastNameEditText);
		emailEditText = (EditText) findViewById(R.id.emailEditText);
		passwordEditText = (EditText) findViewById(R.id.passwordEditText);
		confirmPasswordEditText = (EditText) findViewById(R.id.confirmPasswordEditText);
		signUpButton = (Button) findViewById(R.id.signUpButton);
		cancelButton = (Button) findViewById(R.id.cancelButton);

		signUpButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				validateFields(firstNameEditText, lastNameEditText, emailEditText, passwordEditText);
				if (!error) {
					String firstName = firstNameEditText.getText().toString();
					String lastName = lastNameEditText.getText().toString();
					String email = emailEditText.getText().toString();
					String password = passwordEditText.getText().toString();
					String confirmPassword = confirmPasswordEditText.getText().toString();

					if (password.equals(confirmPassword)) {
						ParseUser user = new ParseUser();
						user.setUsername(email);
						user.setPassword(password);
						user.setEmail(email);
						user.put(PreviewActivity.TB_USER_FIRST_NAME, firstName);
						user.put(PreviewActivity.TB_USER_LAST_NAME, lastName);
						user.signUpInBackground(new SignUpCallback() {

							@Override
							public void done(ParseException e) {
								if (e == null) {
									Toast.makeText(getApplicationContext(), "Signup successful", Toast.LENGTH_SHORT)
											.show();
									Intent intent = new Intent(SignUpActivity.this, AppsActivity.class);
									startActivity(intent);
									finish();
								} else {
									if (ParseException.USERNAME_TAKEN == e.getCode()) {
										Toast.makeText(getApplicationContext(),
												"Account already exist, select different email", Toast.LENGTH_SHORT)
												.show();
									} else {
										Toast.makeText(getApplicationContext(), e.getMessage() + " " + e.getCode(), Toast.LENGTH_SHORT).show();
									}
								}
							}
						});
					} else {
						Toast.makeText(getApplicationContext(), "Password and Confirm Password don't match",
								Toast.LENGTH_SHORT).show();
					}
				}
			}
		});

		cancelButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

	}
}
