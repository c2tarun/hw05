package com.example.hw05.util;

import java.util.Comparator;

import com.example.hw05.model.User;

/*
 * HW05
 * UserComparator.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class UserComparator implements Comparator<User>{

	@Override
	public int compare(User lhs, User rhs) {
		
		return lhs.getLastName().compareTo(rhs.getLastName());
	}

}
