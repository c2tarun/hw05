package com.example.hw05.util;

import java.util.List;

import com.example.hw05.model.User;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/*
 * HW05
 * ParseUtil.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class ParseUtil {
	
	private static final String TAG = ParseUtil.class.getSimpleName();
	public static void getAllUsers(final List<User> users) {
		ParseQuery<ParseObject> userQuery = new ParseQuery<ParseObject>("User");
		userQuery.findInBackground(new FindCallback<ParseObject>() {
			
			@Override
			public void done(List<ParseObject> queriedUsers, ParseException e) {
				for(ParseObject user : queriedUsers) {
					users.add(User.getUserFromParseObject(user));
				}
			}
		});
		
	}

}
