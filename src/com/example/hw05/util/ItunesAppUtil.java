package com.example.hw05.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Xml;

import com.example.hw05.model.ItunesApp;

/*
 * HW05
 * ItunesAppUtil.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class ItunesAppUtil {

	public static class ItunesAppSaxParser extends DefaultHandler {
		List<ItunesApp> appsList;
		ItunesApp app;
		StringBuilder xmlInnerText;
		String imageHeight;

		public static List<ItunesApp> parseItunesApp(InputStream is) throws IOException, SAXException {
			ItunesAppSaxParser parser = new ItunesAppSaxParser();
			Xml.parse(is, Xml.Encoding.UTF_8, parser);
			return parser.getAppsList();
		}

		@Override
		public void startDocument() throws SAXException {
			super.startDocument();
			appsList = new ArrayList<ItunesApp>();
			xmlInnerText = new StringBuilder();
		}

		@Override
		public void endDocument() throws SAXException {
			// TODO Auto-generated method stub
			super.endDocument();
		}

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			if (localName.equals("entry")) {
				app = new ItunesApp();
			} else if (app != null) {

				if (localName.equals("id")) {
					app.setId(attributes.getValue("id"));
				} else if (localName.equals("link")) {
					app.setUrl(attributes.getValue("href"));
				} else if (localName.equals("artist")) {

				} else if (localName.equals("price")) {
					app.setPrice(attributes.getValue("amount"));
				} else if (localName.equals("image")) {
					imageHeight = attributes.getValue("height");
				} else if(localName.equals("releaseDate")) {
					app.setReleaseDate(attributes.getValue("label"));
				}

			}

		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {

			if (localName.equals("entry")) {
				appsList.add(app);
				app = null;
			} else if (app != null) {

				if (localName.equals("title")) {
					app.setTitle(xmlInnerText.toString().trim());
				} else if (localName.equals("artist")) {
					app.setDevName(xmlInnerText.toString().trim());
				} else if (localName.equals("image")) {
					if ("53".equals(imageHeight)) {
						app.setSmallPhoto(xmlInnerText.toString().trim());
					}
					if ("100".equals(imageHeight)) {
						app.setLargePhoto(xmlInnerText.toString().trim());
					}
				}

			}

			xmlInnerText.setLength(0);
		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			xmlInnerText.append(ch, start, length);
		}

		public List<ItunesApp> getAppsList() {
			return appsList;
		}

	}

}
