package com.example.hw05;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.hw05.adapter.AppsListAdapter;
import com.example.hw05.model.ItunesApp;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

/*
 * HW05
 * FavoriteAppsActivity.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class FavoriteAppsActivity extends BaseAppsActivity {

	ListView appsListView;
	List<ItunesApp> favAppsList;
	List<ParseObject> favAppsListParseObjects;
	AppsListAdapter adapter;
	ParseQuery<ParseObject> favAppsQuery;
	private static final String TAG = FavoriteAppsActivity.class.getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_apps);

		appsListView = (ListView) findViewById(R.id.listView1);
		favAppsList = new ArrayList<ItunesApp>();

		favAppsQuery = new ParseQuery<ParseObject>(TB_FAVORITES);
		favAppsQuery.whereEqualTo(TB_FAVORITES_USER, ParseUser.getCurrentUser());
		favAppsQuery.include(TB_FAVORITES_ITUNES_APP);
		favAppsQuery.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(final List<ParseObject> objects, ParseException e) {
				favAppsListParseObjects = objects;
				if (e == null) {
					for (ParseObject object : objects) {
						favAppsList.add(ItunesApp.getAppFromFavoriteParseObject(object));
					}

					adapter = new AppsListAdapter(FavoriteAppsActivity.this, R.layout.app_list_layout, favAppsList);
					appsListView.setAdapter(adapter);
				} else {
					Log.d(TAG, e.getMessage());
				}
			}
		});

		appsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				ItunesApp app = favAppsList.get(position);
				Intent intent = new Intent(FavoriteAppsActivity.this, PreviewActivity.class);
				intent.putExtra(PREVIEW_APP, app);
				startActivityForResult(intent, FAV_APP_ACTIVITY);
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == FAV_APP_ACTIVITY) {

			ItunesApp app = (ItunesApp) data.getExtras().get(PREVIEW_APP);
			adapter.remove(app);
			adapter.notifyDataSetChanged();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void favoritesCleared() {
		adapter.clear();
		adapter.notifyDataSetChanged();
	}
}
