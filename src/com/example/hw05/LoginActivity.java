package com.example.hw05;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

/*
 * HW05
 * LoginActivity.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class LoginActivity extends BaseActivity {

	EditText emailEditText;
	EditText passwordEditText;
	Button loginButton;
	Button signUpButton;

	ParseUser user;
	private static final String TAG = LoginActivity.class.getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		emailEditText = (EditText) findViewById(R.id.emailEditText);
		passwordEditText = (EditText) findViewById(R.id.passwordEditText);
		loginButton = (Button) findViewById(R.id.loginButton);
		signUpButton = (Button) findViewById(R.id.signUpButton);

		//ParseUser.logOut();
		user = ParseUser.getCurrentUser();
		if (user != null) {
			Log.d(TAG,"Logged in as " + user.getString(PreviewActivity.TB_USER_FIRST_NAME));
			forwardToAppsActivity();
		}
		loginButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				validateFields(emailEditText, passwordEditText);
				if (!error) {
					String email = emailEditText.getText().toString();
					String password = passwordEditText.getText().toString();

					ParseUser.logInInBackground(email, password, new LogInCallback() {

						@Override
						public void done(ParseUser loggedInUser, ParseException e) {
							if (e == null) {
								user = loggedInUser;
								Toast.makeText(getApplicationContext(), "Login Successful", Toast.LENGTH_SHORT).show();
								forwardToAppsActivity();
							} else {
								Toast.makeText(getApplicationContext(),
										"Login failed\n Username/Password is incorrect", Toast.LENGTH_LONG).show();
							}
						}
					});
				}
			}
		});

		signUpButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
				startActivity(intent);
				finish();
			}
		});
	}

	private void forwardToAppsActivity() {
		Intent intent = new Intent(LoginActivity.this, AppsActivity.class);
		startActivity(intent);
		finish();
	}
}
