package com.example.hw05;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.hw05.adapter.AppsListAdapter;
import com.example.hw05.model.ItunesApp;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

/*
 * HW05
 * SharedAppsActivity.java
 * Tarun Kumar Mall
 * Pragya Rai
 */
public class SharedAppsActivity extends BaseAppsActivity {
	
	ListView appsListView;
	List<ItunesApp> sharedAppsList;
	List<ParseObject> sharedAppsListParseObjects;
	AppsListAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_apps);
		
		appsListView = (ListView) findViewById(R.id.listView1);
		sharedAppsList = new ArrayList<ItunesApp>();
		
		ParseQuery<ParseObject> sharedAppsQuery = new ParseQuery<ParseObject>(TB_SHARED);
		sharedAppsQuery.whereEqualTo(TB_SHARED_USER, ParseUser.getCurrentUser());
		sharedAppsQuery.include(TB_SHARED_ITUNES_APP);
		sharedAppsQuery.findInBackground(new FindCallback<ParseObject>() {
			
			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				sharedAppsListParseObjects = objects;
				if(e == null) {
					for(ParseObject object : objects) {
						sharedAppsList.add(ItunesApp.getAppFromSharedParseObject(object));
					}
					
					adapter = new AppsListAdapter(SharedAppsActivity.this, R.layout.app_list_layout, sharedAppsList);
					appsListView.setAdapter(adapter);
				}
			}
		});
		
		appsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				ItunesApp app = sharedAppsList.get(position);
				Intent intent = new Intent(SharedAppsActivity.this, PreviewActivity.class);
				intent.putExtra(PREVIEW_APP, app);
				startActivity(intent);
			}
		});
	}
	
	@Override
	protected void sharedCleared() {
		adapter.clear();
		adapter.notifyDataSetChanged();
	}
}
